package main

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

// 函数式编程运用
func fibonacci() func() int {
	previousNumber, nextNumber := 0, 1
	return func() int {
		previousNumber, nextNumber = nextNumber, previousNumber+nextNumber
		return previousNumber
	}
}

type intGenerator func() int

// receiver 是 函数类型
func (generator intGenerator) Read(p []byte) (n int, err error) {
	next := generator()
	s := fmt.Sprintf("%d\n", next)
	return strings.NewReader(s).Read(p)
}

func printFileContent(reader io.Reader) {
	scanner := bufio.NewScanner(reader)

	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func main() {
	f := fibonacci()

	// f() // 1
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())

	// printFileContent()
}

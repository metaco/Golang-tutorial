package fib

func Fibonacci() func() int {
	previousNumber, nextNumber := 0, 1
	return func() int {
		previousNumber, nextNumber = nextNumber, previousNumber+nextNumber
		return previousNumber
	}
}

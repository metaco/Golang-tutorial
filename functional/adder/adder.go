package main

import "fmt"

func adder() func(int) int {
	// TODO 函数体的自由变量， 编译器可以重复叠加自由变量的叠加引用
	sum := 0
	return func(i int) int {
		sum += i
		return sum
	}
}

func main() {
	adder := adder()
	for i := 0; i < 10; i++ {
		fmt.Printf("0 + ... + %d = %d\n", i, adder(i))
	}
}

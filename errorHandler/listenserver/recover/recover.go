package main

import (
	"fmt"
)

func tryRecover() {
	defer func() {
		// TODO 获取panic 中的信息
		r := recover()
		if err, ok := r.(error); ok {
			fmt.Println("Error occurred ", err)
		} else {
			panic(r)
		}
	}()
	b := 0
	a := 5 / b
	fmt.Println(a)
	//panic(errors.New("this is a custom error"))
}

func main() {
	tryRecover()
}

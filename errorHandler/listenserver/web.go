package main

import (
	"learnGolang/errorHandler/listenserver/filelist"
	"log"
	"net/http"
	"os"

	_ "net/http/pprof"
)

type appHandler func(responseWriter http.ResponseWriter, request *http.Request) error

// 处理错误
func errWrapper(handler appHandler) func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		// 保护服务器，处理panic
		defer func() {
			if r := recover(); r != nil {
				log.Printf("panic %v ", r)
				http.Error(writer,
					http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		}()

		err := handler(writer, request)
		if err != nil {
			log.Printf("server occurred error : %s", err.Error())
			code := http.StatusOK
			switch {
			case os.IsNotExist(err):
				code = http.StatusNotFound
			default:
				code = http.StatusInternalServerError
			}
			http.Error(writer, http.StatusText(code), code)
		}
	}
}

func main() {
	// 文件路径从 list 开头
	http.HandleFunc("/", errWrapper(filelist.HandleList))

	err := http.ListenAndServe(":8888", nil)
	if err != nil {
		panic(err)
	}
}

package filelist

import (
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

const prefix = "/list/"

func HandleList(responseWriter http.ResponseWriter, request *http.Request) error {
	// 获取 list 之后的数据

	if strings.Index(request.URL.Path, prefix) != 0 {
		return errors.New("Path must start with /list ")
	}
	path := request.URL.Path[len(prefix):]
	file, err := os.Open(path)
	if err != nil {
		// http.Error(responseWriter, err.Error(), http.StatusInternalServerError)
		return err
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}
	_, _ = responseWriter.Write(bytes)
	return nil
}

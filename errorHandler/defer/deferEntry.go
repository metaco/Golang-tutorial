package main

import (
	"bufio"
	"fmt"
	"learnGolang/functional/fib"
	"os"
)

// defer 函数在完成退出之后，执行defer
//
/**
TODO: defer 用法
Open/Close
Lock/Unlock
*/
func tryDefer() {
	for i := 0; i < 100; i++ {
		defer fmt.Println(i)
		if i == 30 {
			panic("printed too many ")
		}
	}
}

func writeFile(filename string) {
	// If there is an error, it will be of type *PathError.
	file, err := os.OpenFile(filename, os.O_EXCL|os.O_CREATE, 0666)
	if err != nil {
		// panic(err)
		// 错误处理 如果传递的是 error
		if pathError, ok := err.(*os.PathError); !ok {
			panic(err)
		} else {
			// pathError
			fmt.Println(pathError.Op, pathError.Path, pathError.Error())
		}
	}
	// 文件最后关闭
	defer file.Close()

	writer := bufio.NewWriter(file)
	// 最后从buffer 刷掉
	// TODO (参数在defer 语句时计算， defer列表中先进后出)
	//
	defer writer.Flush()

	f := fib.Fibonacci()
	for i := 0; i < 20; i++ {
		fmt.Fprintln(writer, f())
	}
}

func main() {
	writeFile("fib.txt")
	// tryDefer()
}

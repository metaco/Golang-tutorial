package main

import "fmt"

func main() {
	varMaps := map[string]string{
		"key1":   "value",
		"course": "golang",
		"level":  "rank",
	}

	fmt.Println(varMaps)

	// 声明
	makedMap := make(map[string]int) // nil

	var varMap map[string]int // emptyMap

	fmt.Println(makedMap, varMap)

	for mapKey, mapValue := range varMaps {
		fmt.Println(mapKey, mapValue)
	}
	// 获取一个Map 键值
	fmt.Println(varMaps["key1"])

	// 判断一个key 是否存在 keyExisted 为boolean
	if value, keyExisted := varMap["notExistKey"]; keyExisted {
		fmt.Println(value)
	} else {
		fmt.Println("notExistKey access...")
	}
	// fmt.Println(value, keyExisted)

	delete(varMaps, "level")

	value, key := varMaps["level"]
	fmt.Println(value, key)
	fmt.Println(varMaps)

	s := "abcdcefd"
	// []byte 遍历一个 字符串
	// byteMap := make(map[byte]int)
	for index, value := range []byte(s) {
		fmt.Println(index, value)
	}
}

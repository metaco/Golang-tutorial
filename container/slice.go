package main

import "fmt"

func updateSlice(s []int) {
	s[0] = 1111
}

func main() {

	arrayValue := [...]int{1, 2, 3, 4, 5, 6, 7, 8}

	s := arrayValue[2:6]

	fmt.Println("[2:6] = ", s)
	s1 := arrayValue[2:]
	fmt.Println("[2:] =", s1)
	s2 := arrayValue[:6]
	fmt.Println("[:6] =", s2)
	fmt.Println("[:] =", arrayValue[:])

	// slice
	fmt.Println("after updateSlice s1")
	//
	updateSlice(s1)
	fmt.Println(s1)
	fmt.Println(arrayValue)

	// slice 本身没有数据，是数组中的一种视图，传递参数后可以改变操作数组中的元素
	fmt.Println("after updateSlice s2")
	//
	updateSlice(s2)
	fmt.Println(s2)
	fmt.Println(arrayValue)

	// ReSlice 是对 slice 进一步操作
	s2 = s2[:5]
	s2 = s2[2:]

	fmt.Println(s2)

	// slice 的扩展
	s1 = s2[2:4]
	// s1? slice 是对 数组底层的一种视图，所以操作仍是原始数组， 不超过原始数组
	fmt.Println("extending slice")

	fmt.Println("s1", s1)

	// slice 只能向后扩展，不可以向前扩展
	// slice 的实现，ptr, length, capacity
	// ptr 指向第一个元素的位置，length 为元素的长度， capacity的原始数组向后扩展的容量

	fmt.Printf("s1 = %v, length(s1) = %d, cap(s1) = %d\n", s1, len(s1), cap(s1))

	// slice 添加元素

	s3 := append(s1, 10)
	s4 := append(s3, 11)
	s5 := append(s4, 12)

	fmt.Println("s3, s4, s5, arrayValue", s3, s4, s5, arrayValue)
}

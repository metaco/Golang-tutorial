package main

import "fmt"

func printSlice(slices []int) {
	fmt.Printf("len = %d, cap = %d\n", len(slices), cap(slices))
	fmt.Println(slices)
}

func main() {
	// Slice zero value for slice is nil
	var s []int
	// 此时 slice 的值为 nil
	for i := 0; i < 10; i++ {
		printSlice(s)
		s = append(s, 2*i+1)
	}

	// slice 扩展为 2指数级别
	fmt.Println(s)
	// 构建一个 slice 长度为16 ，但是里面的值还不知道
	s1 := make([]int, 16)

	s1 = append(s1, 1)
	// cap 预设置 cap
	s3 := make([]int, 10, 32)
	printSlice(s3)
	// 赋值一段slice
	copy(s1, s)
	printSlice(s1)
	// 删除一段 slice
	s2 := append(s1[:3], s1[4:]...)
	// [0, 3) + [4, ]
	printSlice(s2)
}

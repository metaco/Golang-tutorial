package main

import "fmt"

// Golang 参数传递： 只有值传递一种方式

func swap(a, b *int) {
	*a, *b = *b, *a
}

func swapValue(a, b int) (int, int) {
	return b, a
}

// TODO: 数组是值类型!!! [1]int, [3]int 是不同的类型

func main() {
	a, b := 3, 4
	swap(&a, &b)
	fmt.Println(a, b)

	fmt.Printf("---array--- \n")

	var arrays [3]int
	array2 := [...]int{1, 4, 55}

	for index, value := range arrays {
		fmt.Println(index, value)
	}

	for _, value := range array2 {
		fmt.Println(value)
	}
}

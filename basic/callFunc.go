package main

import (
	"fmt"
	"math"
	"reflect"
	"runtime"
)

func eval(a, b int, operation string) (int, error) {
	switch operation {
	case "+":
		return a + b, nil
	case "-":
		return a - b, nil
	case "*":
		return a * b, nil
	case "/":
		return a / b, nil
	default:
		return -1, fmt.Errorf("unsupported operation %s", operation)
	}
}

// 带余数除法
func division(a, b int) (consequence int, remain int) {
	//remain = a % b
	//consequence = a / b
	//return consequence, remain
	return a / b, a % b
}

/**
函数作为参数传递
*/
func apply(op func(int, int) int, a, b int) int {
	pointer := reflect.ValueOf(op).Pointer()
	pc := runtime.FuncForPC(pointer)
	opName := pc.Name()
	fmt.Printf("calling function %s with args: %d, %d\n", opName, a, b)
	return op(a, b)
}

func power(a, b int) int {
	return int(math.Pow(float64(a), float64(b)))
}

func summary(numbers ...int) int {
	s := 0
	for i := range numbers {
		s += numbers[i]
	}
	return s
}

func main() {
	fmt.Println(eval(3, 4, "*"))
	fmt.Println(division(13, 2))

	if result, err := eval(3, 5, "v"); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("result", result)
	}

	consequence, _ := division(13, 4)
	fmt.Println(consequence)

	fmt.Println(apply(power, 3, 4))

	fmt.Println(summary(1, 5, 7))
}

package main

import (
	"fmt"
	"math"
	"math/cmplx"
)

var (
	packageVariable1 = 3
	packageVariable2 = "kkk"
)

func variable() {
	var variables int
	var strings string
	fmt.Printf("%d %q\n", variables, strings)
}

// 变量赋初值，
func variableInitialize() {
	var a, b int = 3, 4
	var s string = "abc"
	fmt.Println(a, b, s)
}

// 变量类型推断
func variableTypeDeduction() {
	var a, b = 4, false
	var s = "string"

	fmt.Println(a, b, s)
}

// 语法赋值(初始化）
func variableShorter() {
	a, b, c := 3, 4, false
	fmt.Println(a, b, c, packageVariable1, packageVariable2)
}

// 复数操作
func complexVariable() {
	c := 3 + 4i
	abs := cmplx.Abs(c)
	fmt.Println(abs)

	// 验证欧拉公式 e^ i*pi + 1 = 0
	powResult := cmplx.Pow(math.E, 1i*math.Pi) + 1
	fmt.Println(powResult)
}

// TODO: Golang 类型转换是强制的，不出现隐式转换

func triangle() {
	var a, b int = 3, 4
	var c int
	c = int(math.Sqrt(math.Pow(float64(a), 2) + math.Pow(float64(b), 2)))
	fmt.Println(c)
}

/**
枚举，iota 自增赋值枚举
*/
func constant() {
	const (
		php = iota
		java
		python
		cpp
		_
		script
	)

	fmt.Println(php, java, python, cpp, script)

	const (
		b = 1 << (10 * iota)
		kb
		mb
		gb
		tb
		pb
	)

	fmt.Println(b, kb, mb, gb, tb, pb)
}

func main() {
	fmt.Println("hello world")
	variable()
	variableInitialize()
	variableTypeDeduction()
	variableShorter()
	complexVariable()

	triangle()

	constant()
}

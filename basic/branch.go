package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
)

func enumConstant() {
	const (
		b = 1 << (10 * iota)
		kb
		mb
		gb
		tb
		pb
	)

	fmt.Println(b, kb, mb, gb, tb, pb)
}

func readFiles() {
	const filename = "filename.txt"
	if fileContent, err := ioutil.ReadFile(filename); err != nil {
		fmt.Printf("%q\n", err)
	} else {
		// fileContent is byte
		fmt.Printf("fileContent %s\n", fileContent)
	}
}

func convertToBinary(n int) string {
	if 0 == n {
		return "0"
	}

	result := ""
	for ; n > 0; n /= 2 {
		lower := n % 2
		result = strconv.Itoa(lower) + result
	}
	return result
}

func printFile(filename string) {
	fileContent, err := os.Open(filename)
	if err != nil {
		panic(err)
	}

	// 读入一个 scanner
	scanner := bufio.NewScanner(fileContent)

	// 没有起始条件， 结束条件
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func main() {
	fmt.Println("hello world")
	readFiles()
	enumConstant()

	fmt.Println(
		convertToBinary(13),
		convertToBinary(1034),
		convertToBinary(0),
	)

	printFile("filename.txt")
}

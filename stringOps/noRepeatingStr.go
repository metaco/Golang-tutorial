package main

import "fmt"

// 寻找字符串中，不重复的子串最大长度 ,
// 对于每个 字符 x, 查找他最后一次出现的位置 lastOccurredIndex
// 如果 lastOccurredIndex < startIndex ，则忽略
// 如果 lastOccurredIndex >= startIndex 则直接更新 startIndex = lastOccurredIndex + 1
// 更新 lastOccurredIndex, 更新 maxLength

func noRepeatingStrLength(s string) int {
	lastOccurred := make(map[byte]int)
	startIndex := 0
	maxLength := 0
	for index, character := range []byte(s) {
		lastIndex, existed := lastOccurred[character]
		if existed && lastIndex >= startIndex {
			startIndex = lastOccurred[character] + 1
		}
		if index-startIndex+1 > maxLength {
			maxLength = index - startIndex + 1
		}
		lastOccurred[character] = index
	}

	return maxLength
}

func main() {
	fmt.Println(
		noRepeatingStrLength("acdcbc"),
		noRepeatingStrLength("a"),
		noRepeatingStrLength(""),
	)
}

package main

import (
	"fmt"
	"os"
)

func readMaze(filename string) [][]int {
	var row, col int
	file, _ := os.Open(filename)
	_, _ = fmt.Fscanf(file, "%d %d", &row, &col)

	// 读出一行
	maze := make([][]int, row)
	for i := range maze {
		// 读出每一列
		maze[i] = make([]int, col)
		for j := range maze[i] {
			_, _ = fmt.Fscanf(file, "%d", &maze[i][j])
		}
	}

	return maze
}

// 移动方向有 4个, TODO: 上， 左， 下， 右
var direction = [4]Point{
	{-1, 0}, // 上
	{0, -1}, // 左
	{1, 0},  // 下
	{0, 1},  // 右
}

func (p Point) move(direction Point) Point {
	return Point{
		p.i + direction.i,
		p.j + direction.j,
	}
}

// 边界运算检查
func (p Point) atGrid(grid [][]int) (int, bool) {
	// 向上边界检查，向下边界检查
	if p.i < 0 || p.i >= len(grid) {
		return 0, false
	}
	// 每一行的边界检查， 左右边界
	if p.j < 0 || p.j >= len(grid[p.i]) {
		return 0, false
	}

	return grid[p.i][p.j], true
}

type Point struct {
	i, j int
}

// 广度优先算法
func walk(maze [][]int, start, end Point) [][]int {
	// 需要维护一个 带有“路径” 二维 slice
	steps := make([][]int, len(maze))
	for i := range steps {
		steps[i] = make([]int, len(maze[i]))
	}

	// 可探索步骤的坐标队列
	queue := []Point{start}

	for len(queue) > 0 {
		// 探索， 队列头部
		current := queue[0]
		queue = queue[1:]

		// 开始探索
		if current == end {
			break
		}

		// 需要定义探索的方向
		for _, dir := range direction {
			// 新发现的节点, 坐标
			next := current.move(dir)
			// 只有坐标位置是 0 才可以移动， 并且step.Next 位置也是 0 ，
			// 并且 next 的位置不能是 start
			atGrid, bound := next.atGrid(maze)
			// 没有越界， 并且所探索的位置是“墙”, (位置值是1)
			if !bound || atGrid == 1 {
				// 撞墙
				continue
			}

			grid, boundOk := next.atGrid(steps)
			if !boundOk || grid != 0 || next == start {
				continue
			}

			// steps 填充??
			currentCursor, _ := current.atGrid(steps)
			steps[next.i][next.j] = currentCursor + 1

			queue = append(queue, next)
		}
	}

	return steps
}

func main() {
	maze := readMaze("maze/maze.in")
	for _, row := range maze {
		for _, col := range row {
			fmt.Printf("%d ", col)
		}
		fmt.Printf("\n")
	}

	fmt.Printf("-----\n")

	steps := walk(maze, Point{0, 0}, Point{
		len(maze) - 1,
		len(maze[0]) - 1,
	})

	for _, row := range steps {
		for _, val := range row {
			fmt.Printf("%3d", val)
		}
		fmt.Printf("\n")
	}
}

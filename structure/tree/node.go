package tree

import "fmt"

// 结构体定义的结构，方法，
// 首字母大写相对一个包而言可以是
// public , private
type Node struct {
	Value       int
	Left, Right *Node
}

// ?? 局部变量的地址返回至函数调用栈 在 C++ 是典型的错误指令
// golang 内存模型中在返回地址时，均不需要考虑
// 当编译器侦测到变量取到地址返回至调用者时，此时的变量到堆上分配
func CreateNode(value int) *Node {
	return &Node{Value: value}
}

//
func (node Node) Print() {
	fmt.Println(node.Value)
}

/**
	调用者指向不变, 出现TreeNode 变量的一份拷贝
	(等价于
     func setValue(value int , node TreeNode) {
		node.value = value
     }
*/
func (node Node) SetValue(value int) {
	node.Value = value
}

// 调用者指向者, 使用指针作为方法的接收者，（TODO: Golang 没有this 指针)
// TODO nil 指针也可以调用方法
func (node *Node) SetValueForPtr(value int) {
	if node == nil {
		fmt.Println("receiver node was be bil (setting value ignored.)")
		return
	}
	node.Value = value
}

// 如果需要改变变量的内容，必须是指针接收者
// 考虑到结构过大，也需要使用指针接收者
// TODO 考虑到一致性，必须使用指针接收者

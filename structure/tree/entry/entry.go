package main

import (
	"fmt"
	"learnGolang/structure/tree"
)

func main() {
	var root tree.Node

	root = tree.Node{Value: 3}
	root.Left = &tree.Node{}
	root.Right = &tree.Node{5, nil, nil}

	root.Right.Left = new(tree.Node)

	// 工厂函数构建 结构体
	root.Left.Right = tree.CreateNode(4)

	fmt.Println(root,
		root.Right.Value,
		root.Left.Right.Value,
	)

	root.Left.Right.SetValue(76)
	root.Left.Right.Print()
	// value还是4， golang 结构体的函数调用仍然是 值传递
	// 当传递指针时生效，修改生效

	root.Left.Right.SetValueForPtr(28)
	root.Left.Right.Print()
	root.Print()

	// nil 上调用方法
	var pRoot *tree.Node
	pRoot.SetValueForPtr(3)

	pRoot = &root
	pRoot.SetValueForPtr(300)
	pRoot.Print()

	root.Traverse()

	nodeCount := 0
	root.TraverseFunc(func(node *tree.Node) {
		nodeCount++
	})

	// root.PrintTraverse()
	fmt.Println(nodeCount)
}

package tree

import "fmt"

func (node *Node) Traverse() {
	if node == nil {
		return
	}
	// 中序遍历
	node.Left.Traverse()
	node.Print()
	node.Right.Traverse()
}

// 函数遍历
func (node *Node) TraverseFunc(f func(*Node)) {
	if node == nil {
		return
	}
	// 中序遍历
	node.Left.TraverseFunc(f)
	f(node)
	node.Right.TraverseFunc(f)
}

func (node *Node) PrintTraverse() {
	node.TraverseFunc(func(n *Node) {
		n.Print()
	})
	fmt.Println()
}

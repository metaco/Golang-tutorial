package queue

// Slice 的操作可以模拟 队列 [0] 第一个进队元素， [1:] 剩余元素,

type Queue []int

func (queue *Queue) EnQueue(e int) {
	*queue = append(*queue, e)
}

func (queue *Queue) DeQueue() int {
	frontElement := (*queue)[0]
	*queue = (*queue)[1:]
	return frontElement
}

func (queue *Queue) IsEmpty() bool {
	return len(*queue) == 0
}

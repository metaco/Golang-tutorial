package main

import (
	"fmt"
	"learnGolang/structure/queue"
)

func main() {
	q := queue.Queue{1}
	q.EnQueue(2)
	q.EnQueue(3)
	q.EnQueue(10)

	fmt.Println(q.DeQueue())
	fmt.Println(q.DeQueue())

	fmt.Println(q.IsEmpty())
	q.DeQueue()
	q.DeQueue()
	fmt.Println(q.IsEmpty())
}

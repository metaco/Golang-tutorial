package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
)

func main() {
	request, err2 := http.NewRequest(http.MethodGet, "http://www.imooc.com", nil)
	if err2 != nil {
		panic(err2)
	}
	// 获取手机版的imooc
	request.Header.Add("User-Agent",
		"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1")

	// 重定向检查
	client := http.Client{
		CheckRedirect: func(req *http.Request,
			via []*http.Request) error {
			fmt.Println("redirect", req)
			return nil
		},
	}

	response, err := client.Do(request)
	// response, err := http.Get("http://www.imooc.com")
	if err != nil {
		fmt.Printf("%s", err.Error())
	}
	defer response.Body.Close()

	dumpResponse, err := httputil.DumpResponse(response, true)
	if err != nil {
		fmt.Printf("error %s", err.Error())
	}

	fmt.Printf("%s\n", dumpResponse)
}
